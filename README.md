# Using Hashicorp Terraform and Red Hat Ansible with IBM Cloud Hyper Protect Services
This repository is a demo of using [Hashicorp](https://hashicorp.com) [Terraform](https://terraform.io)
and [Red Hat](https://www.redhat.com) [Ansible](https://www.nsible.com) to provision infrastructure on
[IBM Cloud](https://cloud.ibm.com) and configure [Nextcloud](https://www.nextcloud.com) to be deployed
with almost no manual intervention.

This demo makes use of two IBM Cloud services:
- [Hyper Protect Virtual Server](https://www.ibm.com/cloud/hyper-protect-virtual-servers) used to run the Web Server.
- [Hyper Protect Database-as-a-Service for PostgreSQL](https://www.ibm.com/cloud/hyper-protect-dbaas) used to host the Nextcloud database.

## Prerequisites

In order to use these playbooks, the following prerequisites have to be met:
- an [IBM Cloud](https://cloud.ibm.com) account is available.
  There are no specific requirements for the type of account.
- the ibmcloud CLI is configured following the [Getting Started](https://cloud.ibm.com/docs/cli?topic=cli-getting-started) instructions.
- both the [hpvs and dbaas CLI plugins](https://cloud.ibm.com/docs/cli?topic=cli-plug-ins) are installed.

 ## Demo scenario

 The demo scenario is depicted below:

 ![Demo scenario](img/scenario.png "Using Terraform and Ansible with IBM Cloud Hyper Protect Services demo scenario")

### Step 0 - Demo setup

To be able to run this demo from your own laptop, the following setup steps must be followed.

#### Install and configure terraform
[Terraform](https://www.terraform.io) is used to create the infrastructure in the IBM Cloud.

Once terraform is installed using your favorite package manager, the IBM Cloud provider for Terraform needs to be installed and configured.
In my environment, you'll find a file named [provider.tf](terraform/provider.tf) in the terraform directory, with the following content:

```
variable "ibmcloud_api_key" {}

terraform {
  required_providers {
    ibm = {
      source  = "IBM-Cloud/ibm"
      version = "1.25.0"
    }
  }
}

provider "ibm" {
  ibmcloud_api_key   = var.ibmcloud_api_key
}
```
The source points to the [Terraform Registry](https://registry.terraform.io/providers/IBM-Cloud/ibm/latest) where the provider is maintained.
Once the file is updated to fit your needs, Terraform needs to be initialized:

```
$ terraform init -upgrade

Initializing the backend...

Initializing provider plugins...
- Finding ibm-cloud/ibm versions matching "1.25.0"...
- Installing ibm-cloud/ibm v1.25.0...
- Installed ibm-cloud/ibm v1.25.0 (self-signed, key ID AAD3B791C49CC253)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has made some changes to the provider dependency selections recorded
in the .terraform.lock.hcl file. Review those changes and commit them to your
version control system if they represent changes you intended to make.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```
***NOTE***: If you initialize Terraform for the first time, remove the -upgrade option.

The IBM Cloud API key is stored in a separate file, `terraform.tfvars` with the following content:

```
$ cat terraforms.tfvars
ibmcloud_api_key = "ThisIsAnIBMCLoudAPIKeyYouWillNeedYourOwnKeys"
```
For more information about how to create an API key in IBM Cloud, please refer to [this page](https://cloud.ibm.com/docs/account?topic=account-userapikey).

#### Global variables definition file

***NOTE:*** For more information about how Ansible manages variables, please refer to [the following page](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html).

I chose to keep all user modifiable inputs in a single place.
The file, all, is located in the group_vars folder, wich content is shown below:

```
---
nc_version: "20.0.8"
hpdbaas_user: "ncadmin"
hpvs_location: "syd05"
hpdbaas_location: "au-syd"
hpdbaas_password: "MySuperSafePassw0rd!"
ibmcloud_api_key: "ThisIsAnIBMCLoudAPIKeyYouWillNeedYourOwnKeys"
```
- nc_version sets the version of Nextcloud to install
- hpdbaas_user is the name of the uer which will be created as part of the provisioning of the HPDBaaS instance.
- hpvs_location is the IBM Cloud location to use for provisioning the HPVS instance. In my case, that's Sydney.
- hpdbaas_location is the IBM Cloud location to use for provisioning the HPDBaaS instance. In my case, that's Sydney.
- hpdbaas_password is the password set for the hpdbaas_user.

To view the list of IBM Cloud location for HPVS, one can use the followinig command:
```
$ ibmcloud catalog service hpvs
Getting catalog entry...
OK

ID                 986f2197-9f9a-44f4-9463-f17ec64c6729
Name               hpvs
Kind               service
Provider           IBM
Tags               free, ibm_created, rc_compatible, virtualservers, vm
Active             true
Description        Create and run LinuxONE-based virtual servers with exclusive access to your data and complete authority over your sensitive workloads.
Bindable           false
Original Name
RC Compatible      true
RC Provisionable   true
IAM Compatible     true
Children           Name                 Kind         ID                                                Location   Original Location   Target
[...]
                   lite-s               plan         bb0005a1-ec13-4ee4-86f4-0c3b15a357d5
                   |__lite-s-dal10-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:dal1067057   dal10                          bluemix-dal10
                   |__lite-s-dal12-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:dal1258855   dal12                          bluemix-dal12
                   |__lite-s-dal13-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:dal1362000   dal13                          bluemix-dal13
                   |__lite-s-fra02-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:fra0273381   fra02                          bluemix-fra02
                   |__lite-s-fra04-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:fra0436074   fra04                          bluemix-fra04
                   |__lite-s-fra05-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:fra0563817   fra05                          bluemix-fra05
                   |__lite-s-syd01-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:syd0136943   syd01                          bluemix-syd01
                   |__lite-s-syd04-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:syd0438854   syd04                          bluemix-syd04
                   |__lite-s-syd05-rc   deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:syd0527267   syd05                          bluemix-syd05
                   |__lite-s-wdc04      deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:wdc0418999   wdc04                          bluemix-wdc04
                   |__lite-s-wdc06      deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:wdc0699029   wdc06                          bluemix-wdc06
                   |__lite-s-wdc07      deployment   bb0005a1-ec13-4ee4-86f4-0c3b15a357d5:wdc0718642   wdc07                          bluemix-wdc0
```

### Step 1 - IBM Cloud

In the step, I usually show the IBM Cloud UI and the services I am going to use:
- Hyper Protect Virtual Server
- Hyper Protect Database-as-a-Service for PostgreSQL

I also highlights the inputs that are required for HPVS and HPDBaaS, as I will later show them in the IDE.

### Step 2 - Infrastructure provisionning setup

In this step of the demo, I show the files that are used as inputs for Terraform:
- [provider.tf](terraform/provider.tf) holds the configuration information for IBM Cloud Terraform provider
- [infra.tf.j2](templates/infra.tf.j2) is the Jinja2 template used to create the infra.tf file used by Terraform. The variables between {{ }} are populated by Ansible as part of the playbook execution.

### Step 3 - Infrastructure provisionning - calling Terraform from Ansible

Here, I am using a *local* Ansible playbook to provision the infrastructure: [0_nextcloud_terraform.yml](0_nextcloud_terraform.yml).\
The playbook create the required input files for Terraform, then delegates the provisionning to Terraform.\
Last steps include calls to ibmcloud CLI to retrieve the HPVS Public IP address and HPDBaaS host and port.

```shell
$ ansible-playbook 0_nextcloud_terraform.yml
```

**Note**: An [IBM Cloud API key](https://cloud.ibm.com/iam/apikeys) is required for Terraform to interact with IBM Cloud.

**Note**: Prior to running the demo, make sure to have a valid authentication token with IBM Cloud.\
Failure to do so will result in an empty inventory and failures to execute the next playbooks.

### Step 4 - Ansible setup

In this step, I briefly introduce the IDE environment, the filesystem structure, where to the find the different files.

### Step 5 - Infrastructure configuration

This is a two steps process.

#### Configuration as the *root* user

Once provisioned, only the *root* user is available in the HPVS host.\
Playbook [1_nextcloud_root_config.yml](1_nextcloud_root_config.yml) is used to perform the following configuration actions:
 - configure sudo for my unprivileged user to execute privileged commands without passwords
 - create an unprivileged user
 - add my SSH public key into the newly created user `$HOME/.ssh/authorized_keys` file

```shell
$ ansible-playbook -i inventory 1_nextcloud_root_config.yml -u root
```

***Note***: Right now, the user is hardcoded, but can easily be made a variable and added into the group_vars/all file.

#### Configuration as a general user

Once the unprivileged user is created, playbook [2_nextcloud.yml](2_nextcloud.yml) can be executed to finalize the configuration.\
This playbook is made of 2 roles:
- the common role is reponsible for:
    - hardening the SSH configuration, following recommendations from [Stribika](https://stribika.github.io/2015/01/04/secure-secure-shell.html) and [Mozilla Infosec](https://infosec.mozilla.org/guidelines/openssh.html)
    - runs `apt dist-upgrade` to get all latest packages installed
    - installs a few essential packages
- the nextcloud role then installs the Nextcloud application itself:
    - configures the PostGreSQL database
    - installs PHP prerequisite packages
    - installs and configure the Apache webserver
    - configures firewall
    - downloads and extracts the Nextcloud archive

```shell
$ ansible-playbook -i inventory 2_nextcloud.yml -u guigui
```

### Step 6 - Configure NextCloud

Last part of the demo requires log in to the IP address assigned to the HPVS machine to check the Nextcloud configuration page is shown.
The IP to connect to can be found:
- in the inventory file
- or as the output of the following command (assuming there is only one HPVS instance provisioned)
     ```shell
     $ ibmcloud hpvs instances | grep Public\ IP | awk '{print $4}'
     135.90.70.164
    ```
Using a web browser to connect to the URL http://<HPVS_IP>/nextcloud should display the application installation page:
![Nextcloud Install](img/NC_Install.png "Nextcloud 20 installation screen")
